# Ansible Configuration Management for Slowb.ro

Deployment scripts used for slowb.ro. Uses:

- acme.sh to generate the LE certs via he.net for DNS
- traefik for web ingress & routing
- mailcow for e-mail hosting
- wallabag as a read-it-later app 

## App Deployment


### Mailcow

```
ansible-playbook playbook_remote_mailcow.yml -i hosts --ask-become-pass
```

### Wallabag
```
ansible-playbook playbook_remote_wallabag.yml -i hosts --ask-become-pass
```
